module.exports = {
  devServer: {
    port: 8888,
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "./node_modules/pn-design-assets/pn-assets/styles/_pn-typography.scss";
          @import "./node_modules/pn-design-assets/pn-assets/styles/_pn-resets.scss";
          @import "./node_modules/pn-design-assets/pn-assets/styles/_pn-colors.scss";
        `,
      },
    },
  },
};
