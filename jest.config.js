const { jestConfig } = require("@postnord/code-unify-utils");

module.exports = jestConfig({
  snapshotSerializers: ["jest-serializer-vue"],
  setupFiles: ["<rootDir>/__tests__/setup.js"],
});
