import { createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import Vuex from "vuex";

export default () => {
  const localVue = createLocalVue();

  localVue.use(Vuex);
  const store = new Vuex.Store({
    state: {
      lang: "en",
      loggedIn: false,
    },
  });

  localVue.use(VueRouter);
  const router = new VueRouter();

  return { localVue, router, store };
};
