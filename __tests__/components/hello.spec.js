import { shallowMount } from "@vue/test-utils";
import template from "@/components/HelloWorld";
import createMocks from "@/../__tests__/createMocks";

describe("template.vue", () => {
  let mocks = {};
  beforeEach(() => {
    mocks = createMocks();
  });

  it("mount", () => {
    const wrapper = shallowMount(template, { ...mocks });
    expect(wrapper.element).toMatchSnapshot();
  });

  it("sets props values", () => {
    const wrapper = shallowMount(template, {
      ...mocks,
      propsData: { msg: "12345678" },
    });

    expect(wrapper.props().msg).toBe("12345678");
  });
});
