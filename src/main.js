import Vue from "vue";
import App from "@/App.vue";
import router from "@/router";
import store from "@/store.js";
import {
  applyPolyfills,
  defineCustomElements,
} from "@postnord/web-components/loader";

import "../node_modules/pn-design-assets/pn-assets/styles/_pn-fonts.scss";

Vue.config.productionTip = false;
Vue.config.ignoredElements = [/^pn-/];
applyPolyfills().then(() => defineCustomElements());

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");
