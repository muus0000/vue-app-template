import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    lang: "en",
    loggedIn: false,
  },
  actions: {
    changeLanguage(context, payload) {
      context.commit("changeLanguage", payload);
    },
  },
  mutations: {
    changeLanguage(state, payload) {
      return (state.lang = payload.lang);
    },
  },
});
