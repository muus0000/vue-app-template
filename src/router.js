import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/contact1",
    name: "contact",
    component: () => import("./components/contact.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: __dirname,
  routes: routes,
});

export default router;
