# Vue JS template

Includes:

- linter
- prettier
- vue / router / vuex store
- husky for precommit
- vue-jest setup
- PN Web components
- PN assets
